﻿using BookingNotifications.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;

namespace BookingNotifications
{
	public partial class BookingNotifications : ServiceBase
	{
		private Logger _logger = LogManager.GetLogger("BookingToolsLogger");

		private CancellationTokenSource _Cts = new CancellationTokenSource();

		private Task _MainTask = null;

		public BookingNotifications()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			_logger.Info("Starting the Notifications service...");

			_MainTask = new Task(ProcessNotifications, _Cts.Token, TaskCreationOptions.LongRunning);

			_MainTask.Start();
		}

		private void ProcessNotifications()
		{
			_logger.Info("Starting the BookingNotifications Service...");

			CancellationToken cancellation = _Cts.Token;

			TimeSpan interval = TimeSpan.Zero;

			// 48 Hour Notifications
			var initializer = new NotificationInitializer
			{
				Logger = _logger,
				ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
				PollingInterval = Convert.ToInt32(ConfigurationManager.AppSettings["PollingIntervalInSeconds"].ToString()),
				UserName = ConfigurationManager.AppSettings["EZTextUserName"].ToString(),
				Password = ConfigurationManager.AppSettings["EZTextPassword"].ToString(),
				TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]),
				TestPhoneNumber = ConfigurationManager.AppSettings["TestPhoneNumber"],
				MessageTypeID = ConfigurationManager.AppSettings["EZTextMessageTypeID"].ToString(),
				ApiBaseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"].ToString(),
				ProcessorQuery = string.Format("select NotificationID, Phone, Email, NotificationType, RequestMessage from dbo.Notifications where Status = {0} and LastUpdated IS NULL",
							(int)NotificationStatus.Pending),
				ByPassSmsSendReturnSuccess = Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassSmsSendReturnSuccess"].ToString())
			};

			while (!cancellation.WaitHandle.WaitOne(interval))
			{
				try
				{
					_logger.Info("Begin processing notifications...");

					#region setup the Notification Processor

					var notificationProcessor = new NotificationsProcessor(initializer);

					notificationProcessor.ProcessNotifications();

					#endregion setup the Notification Processor

					// Occasionally check the cancellation state.
					if (cancellation.IsCancellationRequested)
					{
						break;
					}
					interval = new TimeSpan(0, 0, initializer.PollingInterval);

					_logger.Info("Successfully processed batch.");
				}
				catch (Exception ex)
				{
					_logger.Error("There were exceptions caught in processing the last batch.", ex);
					interval = new TimeSpan(0, 0, initializer.PollingInterval);
				}
			}
		}

		protected override void OnStop()
		{
			_logger.Info("Stopping BookingNotificationService...");

			_Cts.Cancel();
			_MainTask.Wait();

			_logger.Info("Stopped BookingNotificationService.");
		}
	}
}
