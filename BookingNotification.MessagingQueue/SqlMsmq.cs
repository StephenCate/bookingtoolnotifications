﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;

public class SqlMsmq
{
	public SqlMsmq()
	{

	}

	public void SendMessage(string queuePath, string messageLabel, string messageBody)
	{
		if (!MessageQueue.Exists(queuePath))
		{
			MessageQueue.Create(queuePath);
		}

		MessageQueue mq = new MessageQueue(queuePath);
		Message mm = new Message();
		mm.Label = messageLabel;
		mm.Body = messageBody;
		mq.Send(mm);
		mq.Close();
	}
}