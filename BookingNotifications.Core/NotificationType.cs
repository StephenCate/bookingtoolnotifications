﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookingNotifications.Core
{
	public enum NotificationType
	{
		ScheduleConfirmation = 1,
		FortyEightHourReminder = 2,
	}
}
