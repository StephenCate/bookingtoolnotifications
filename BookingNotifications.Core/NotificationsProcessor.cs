﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using NLog;
using RestSharp;
using System.Globalization;

namespace BookingNotifications.Core
{
	public class NotificationsProcessor : INotificationProcessor
	{
		public string ConnectionString { get; set; }

		public ILogger Logger { get; set; }

		public string QueueName { get; set; }

		public string UserName { get; set; }

		public string Password { get; set; }

		public bool TestMode { get; set; }

		public string TestPhoneNumber { get; set; }

		public string ApiBaseUrl { get; set; }

		public string MessageTypeID { get; set; }

		public string ProcessorName { get; set; }

		public string ProcessQuery { get; set; }

		public bool ByPassSmsSendReturnSuccess { get; private set; }

		private object CurrentLock = new Object();

		public NotificationsProcessor(NotificationInitializer initializer)
		{
			this.ConnectionString = initializer.ConnectionString;
			this.Logger = initializer.Logger;
			this.TestMode = initializer.TestMode;
			this.TestPhoneNumber = initializer.TestPhoneNumber;
			this.ApiBaseUrl = initializer.ApiBaseUrl;
			this.MessageTypeID = initializer.MessageTypeID;
			this.UserName = initializer.UserName;
			this.Password = initializer.Password;
			this.ProcessQuery = initializer.ProcessorQuery;
			this.ByPassSmsSendReturnSuccess = initializer.ByPassSmsSendReturnSuccess;
		}

		private void ProcessFortyEightHourReminder()
		{
			try
			{
				using (var connection = new SqlConnection(this.ConnectionString))
				{
					connection.Open();

					// Create a new SqlCommand object.
					using (SqlCommand command = new SqlCommand(this.ProcessQuery,connection))
					{
						var results = new List<NotificationResponse>();

						var isProcessed = false;

						// Execute the command.
						using (SqlDataReader reader = command.ExecuteReader())
						{
							this.Logger.Info("Querying records to update...");

							isProcessed = SendNotifications(reader, ref results);
						}

						if (isProcessed)
						{
							this.Logger.Info("Updating DB records for last run...");

							UpdateNotificationRecords(connection, results);
						}
					}
				}

			}
			catch (Exception e)
			{
				this.Logger.Error(e,"Errors encounted on ProcessFortyEightHourReminder");
			}

		}

		private void UpdateNotificationRecords(SqlConnection connection, List<NotificationResponse> results)
		{
			
			if (results.Any())
			{
				DataTable updateTable = new DataTable();

				updateTable.Columns.Add(new DataColumn { ColumnName = "LastUpdated", DataType = typeof(string) });
				updateTable.Columns.Add(new DataColumn { ColumnName = "Status", DataType = typeof(int) });
				updateTable.Columns.Add(new DataColumn { ColumnName = "ResponseMessage", DataType = typeof(string) });
				updateTable.Columns.Add(new DataColumn { ColumnName = "NotificationID", DataType = typeof(Guid) });

				results.ForEach((r) => {
					DataRow row = updateTable.NewRow();

#if DEBUG
					row["LastUpdated"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
#else
					row["LastUpdated"] = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
#endif
					row["Status"] = (int)r.Status;
					row["ResponseMessage"] = r.Response;
					row["NotificationID"] = r.NotificationID;

					updateTable.Rows.Add(row);

					row.AcceptChanges();

					row.SetModified();

				});

				SqlDataAdapter adapter = new SqlDataAdapter();

				var query = "UPDATE Notifications SET Status = @Status, ResponseMessage = @Response, LastUpdated = @Now WHERE NotificationID = @NotificationID";

				SqlCommand command = new SqlCommand(query, connection);

				command.Parameters.Add("@Now", SqlDbType.DateTime2, 20, "LastUpdated");

				command.Parameters.Add("@Status", SqlDbType.Int,1, "Status");
					
				command.Parameters.Add("@Response", SqlDbType.VarChar, 500, "ResponseMessage");
				
				command.Parameters.Add("@NotificationID", SqlDbType.UniqueIdentifier, 50, "NotificationID");

				command.UpdatedRowSource = UpdateRowSource.None;

				adapter.UpdateCommand = command;

				adapter.UpdateBatchSize = 100;

				adapter.ContinueUpdateOnError = true;

				adapter.Update(updateTable);
			}

			this.Logger.Info("Finished updating {0} DB records from the latest query.", results.Count());
		}

		private bool SendNotifications(SqlDataReader reader, ref List<NotificationResponse> results)
		{
			var result = true;

			if (!reader.HasRows)
			{
				this.Logger.Info("There are no Notifications pending.");

				return false;
			}

			while (reader.Read())
			{
				try
				{
					var apiClient = new RestClient(this.ApiBaseUrl);

					var request = new RestRequest(Method.POST);
					request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
					request.AddParameter("User", this.UserName);
					request.AddParameter("Password", this.Password);

					var phoneNumber = this.TestMode ? this.TestPhoneNumber : reader["Phone"];
					request.AddParameter("PhoneNumbers", phoneNumber);
					request.AddParameter("Message", reader["RequestMessage"]);
					request.AddParameter("MessageTypeID", this.MessageTypeID);

					IRestResponse response;
					if (this.ByPassSmsSendReturnSuccess)
					{
						response = new RestResponse();
						response.StatusCode = System.Net.HttpStatusCode.OK;
						response.Content = "Mock API Call Successful";
					}
					else
					{
						response = apiClient.Execute(request);
					}
					

					if (response.StatusCode == System.Net.HttpStatusCode.Created || response.StatusCode == System.Net.HttpStatusCode.OK)
					{
						results.Add(new NotificationResponse { Status = NotificationStatus.Succeeded, NotificationID = Guid.Parse(reader["NotificationID"].ToString()), Response = response.Content });
					}
					else
					{
						this.Logger.Error(response.ErrorException, "Request failed for NotificationID = {0}. Api Response: {1}", reader["NotificationID"].ToString(), response.ErrorMessage);

						results.Add(new NotificationResponse { Status = NotificationStatus.Failed, NotificationID = Guid.Parse(reader["NotificationID"].ToString()), Response = response.ErrorMessage });
					}
				}
				catch (Exception e)
				{
					this.Logger.Error(e, "Error sending notification for NotificationID = {0}.", reader["NotificationID"].ToString());

					results.Add(new NotificationResponse { Status = NotificationStatus.Pending, NotificationID = Guid.Parse(reader["NotificationID"].ToString()), Response = e.Message});
				}
			}
			
			this.Logger.Info("Finished sending SMS notifications from the latest query.");

			return result;
		}

		public void ProcessNotifications()
		{
			ProcessFortyEightHourReminder();
		}
	}
}

