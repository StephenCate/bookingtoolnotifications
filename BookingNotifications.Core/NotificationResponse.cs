﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingNotifications.Core
{
	public class NotificationResponse
	{
		public NotificationStatus Status { get; set; }

		public Guid NotificationID { get; set; }

		public string Response { get; set; }
	}
}
