﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingNotifications.Core
{
	public class NotificationInitializer
	{
		public ILogger Logger { get; set; }

		public string ConnectionString { get; set; }

		public int	PollingInterval { get; set; }

		public string UserName { get; set; }

		public string Password { get; set; }

		public bool TestMode { get; set; }

		public string TestPhoneNumber { get; set; }

		public string ApiBaseUrl { get; set; }

		public string MessageTypeID { get; set; }

		public string ProcessorQuery { get; set; }

		public bool ByPassSmsSendReturnSuccess { get; set; }

	}
}
