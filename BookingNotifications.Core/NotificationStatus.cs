﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookingNotifications.Core
{
	public enum NotificationStatus
	{
		Pending = 1,
		Succeeded = 2,
		Failed = 3
	}
}
