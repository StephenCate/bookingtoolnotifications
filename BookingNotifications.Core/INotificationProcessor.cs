﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookingNotifications.Core
{
	public interface INotificationProcessor
	{
		void ProcessNotifications();
	}
}
