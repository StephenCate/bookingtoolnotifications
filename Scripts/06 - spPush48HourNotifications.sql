USE [BookingTool]
GO

/****** Object:  StoredProcedure [dbo].[spPush48HourNotifications]    Script Date: 02/02/2016 07:25:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPush48HourNotifications]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPush48HourNotifications]
GO

USE [BookingTool]
GO

/****** Object:  StoredProcedure [dbo].[spPush48HourNotifications]    Script Date: 02/02/2016 07:25:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Stephen Cate
-- Create date: 1/23/2016
-- Description:	Pushes Notifications in the queue for people who should receive a text message 48-hrs before their schedule
-- =============================================
CREATE PROCEDURE [dbo].[spPush48HourNotifications]
AS
BEGIN

	PRINT 'Begin pulling records from BookingClaims for the following criteria:'
	PRINT '			SubmissionStatus IN ("RESCHEDULED", "RESERVED")'
	PRINT '			bc.AllowTextMessage = "Yes"'
	PRINT '			bc.MobilePhoneNumber IS NOT NULL AND bc.MobilePhoneNumber <> '''
	PRINT '			ScheduleDate < DATEADD(day, 2, GETDATE())'
	PRINT '			ScheduleDate > "2016-02-01 00:00:00.000"'

	DECLARE @NotificationsStart VARCHAR(50)
	SET @NotificationsStart = '2016-02-01 00:00:00.000'

	INSERT INTO dbo.Notifications
	(NotificationID, BookingClaimID, Phone, Email, NotificationType, [Status],RequestMessage,ResponseMessage,RequestID,CreatedBy,Created,LastUpdated)
	SELECT NEWID()
			,bc.BookingClaimID
			, dbo.fnGetFormattedPhone(bc.PhoneNo) as PhoneNo
			,Email
			,2 as NotificaitonType
			,1 as [Status]
			,dbo.fnGet48HourFormattedMessage(bpm.ProgramName, ScheduleDate, ScheduleTime, BookingClaimId) as RequestMessage
			,NULL as ResponseMessage
			,NULL as RequestID
			,bc.UpdatedBy as CreatedBy
			,GETDATE() as Created
			,NULL as LastUpdated
	FROM [BookingClaims] bc
	JOIN BookingProgramMaster bpm ON bc.ProgramID = bpm.BookingProgramId
	WHERE 1=1
	AND dbo.fnGetStatus(bc.BookingClaimID, 'SUBMISSION') IN ('RESCHEDULED', 'RESERVED')
	AND bc.AllowTextMessage = 'Yes'
	AND (bc.MobilePhoneNumber IS NOT NULL AND bc.MobilePhoneNumber <> '')
	AND DATEADD(MINUTE, DATEDIFF(MINUTE, 0, CAST(COALESCE(ScheduleTime, '00:00') as datetime)), ScheduleDate) BETWEEN @NotificationsStart AND  DATEADD(DAY,2,GETDATE())
	AND NOT EXISTS(SELECT n.NotificationID FROM Notifications n WHERE n.BookingClaimID = bc.BookingClaimId)
	ORDER BY ScheduleDate ASC

	PRINT '' + CONVERT(VARCHAR,@@ROWCOUNT) + ' has been added to the Notifications Queue.'

END





GO


