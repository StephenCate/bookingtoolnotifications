USE [BookingTool]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGetFormattedPhone]    Script Date: 24/01/2016 7:55:45 PM ******/
DROP FUNCTION [dbo].[fnGetFormattedPhone]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGetFormattedPhone]    Script Date: 24/01/2016 7:55:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Stephen Cate
-- Create date: 1/23/2016
-- Description:	Formats the phone number to remove the following characters: '(',')','-',' '
-- =============================================
CREATE FUNCTION [dbo].[fnGetFormattedPhone]
(
	-- Add the parameters for the function here
	@PhoneNumber VARCHAR(100)
)
RETURNS VARCHAR(15)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Output VARCHAR(15)

	-- Add the T-SQL statements to compute the return value here
	SET @Output = REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(@PhoneNumber)), ' ',''), '(',''),')',''),'-','')

	-- Return the result of the function
	RETURN @Output

END

GO


