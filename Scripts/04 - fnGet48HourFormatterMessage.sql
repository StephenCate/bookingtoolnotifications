USE [BookingTool]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGet48HourFormattedMessage]    Script Date: 25/01/2016 12:00:43 AM ******/
DROP FUNCTION [dbo].[fnGet48HourFormattedMessage]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGet48HourFormattedMessage]    Script Date: 25/01/2016 12:00:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Stephen Cate
-- Create date: 1/23/2016
-- Description:	Returns the 48 hour message for the recipient
-- =============================================
CREATE FUNCTION [dbo].[fnGet48HourFormattedMessage]
(
	-- Add the parameters for the function here
	@ProgramName VARCHAR(500),
	@ScheduleDate datetime,
	@ScheduleTime VARCHAR(50),
	@ClaimID BIGINT
)
RETURNS VARCHAR(150)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Output VARCHAR(150)

	DECLARE @Date VARCHAR(15)

	SET @Date = CONVERT(VARCHAR, CAST(@ScheduleDate as date), 101) 

	DECLARE @ProgramAbbreviation VARCHAR(15)

	SELECT @ProgramAbbreviation = dbo.fnGetProgramAbbreviation(@ProgramName)

	DECLARE @ClaimIDText VARCHAR(50)

	SET @ClaimIDText = CONVERT(VARCHAR, @ClaimID)



	-- Add the T-SQL statements to compute the return value here
	SET @Output = 'This is your 48 hour reminder for your ' + @ProgramAbbreviation  + ' appointment at ' + @ScheduleTime + ' on ' + @Date + '. '
							+ 'If you want to re-book, contact 1-888-316-8014 Claim ID ' + @ClaimIDText

	-- Return the result of the function
	RETURN @Output

END




GO


