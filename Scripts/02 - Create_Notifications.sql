USE [BookingTool]
GO

/****** Object:  Table [dbo].[Notifications]    Script Date: 24/01/2016 7:53:18 PM ******/
DROP TABLE [dbo].[Notifications]
GO

/****** Object:  Table [dbo].[Notifications]    Script Date: 24/01/2016 7:53:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Notifications](
	[NotificationID] [uniqueidentifier] NOT NULL,
	[BookingClaimID] [bigint] NULL,
	[Phone] [varchar](20) NOT NULL,
	[Email] [varchar](150) NULL,
	[NotificationType] [int] NULL,
	[Status] [int] NOT NULL,
	[RequestMessage] [nvarchar](max) NOT NULL,
	[ResponseMessage] [nvarchar](max) NULL,
	[RequestID] [nvarchar](100) NULL,
	[CreatedBy] [bigint] NULL,
	[Created] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


