USE [BookingTool]
GO

/****** Object:  StoredProcedure [dbo].[gspBookingDispatch_AdministerHistory]    Script Date: 28/01/2016 1:16:53 AM ******/
DROP PROCEDURE [dbo].[gspBookingDispatch_AdministerHistory]
GO

/****** Object:  StoredProcedure [dbo].[gspBookingDispatch_AdministerHistory]    Script Date: 28/01/2016 1:16:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[gspBookingDispatch_AdministerHistory] (
	@LoginUserId BIGINT = 0
	, @ClaimId BIGINT = 0
	, @TechnicianId BIGINT = 0
	, @StartDate DATETIME = NULL
	, @EndDate DATETIME = NULL
	, @programid BIGINT = 0
	, @datefilter NVARCHAR(2) = '2' --Created on
)
AS
BEGIN
	IF (@datefilter = 2)
		BEGIN
			SELECT a.BookingClaimId
				, claim.ConsumerEnergyAccountNumber AS 'GasUtilityAccountNo'
				, CASE WHEN b.Status IS NULL THEN 'Scheduled' WHEN b.Status = 0 THEN 'FT Assigned' ELSE 'Dispatched' END AS [ScheduleType]
				--, CASE WHEN b.Status IS NULL THEN 'Scheduled' WHEN b.Status = 0 THEN 'FT Assigned' ELSE 'Dispatched' END AS [Status]
				, claim.Basement
				, claim.ScheduleDate
				, claim.ScheduleTime
				, ISNULL(u1.FirstName, '') + ' ' + ISNULL(u1.LastName, '') AS [Group]
				, claim.PhoneNo
				, ISNULL(b.[Status], 0) AS [Status]
				, ISNULL(claim.FirstName, '') + ' ' + ISNULL(claim.LastName, '') AS [Client]
				, ISNULL(b.FirstTechnicianId, 0) AS [FirstTechnicianId] --Quick patch to eliminate duplicate booking dispatch
				, b.SecondTechnicianId --Quick patch to eliminate duplicate booking dispatch
				, a.BookingDispatchID --Quick patch to eliminate duplicate booking dispatch
				, b.CreatedOn
				, ISNULL(status.Name, 'Inactive') AS 'DispatchStatusDesc'
				, ISNULL(status.IsPositive, 0) AS 'IsPositive'
				, claim.FTLogFile
				, claim.ProgramID
				, pm.ProgramName

				, claim.BRNumber
				, claim.PremiseID
				, claim.AccountType
				, claim.HEAParticipation
				, claim.IQParticipation
				, claim.EaseOnly
				, claim.PriorParticipationDate
				, n.[Status] AS NotificationStatus
			FROM (
				SELECT MAX(bd.BookingDispatchId) AS [BookingDispatchId]
					, bc.BookingClaimId
				FROM BookingClaims bc
				LEFT JOIN BookingDispatch bd ON bd.BookingClaimId = bc.BookingClaimId
				
				GROUP BY bc.BookingClaimId
				) a
			LEFT JOIN BookingClaims claim ON claim.BookingClaimId = a.BookingClaimId
			LEFT JOIN BookingDispatch b ON b.BookingDispatchId = a.BookingDispatchId
			LEFT JOIN BookingTechnician ft1 ON ft1.TechnicianId = b.FirstTechnicianId
			LEFT JOIN Users u1 ON u1.UserId = ft1.UserId
			LEFT JOIN BookingStatus status ON status.StatusId = b.Status
			LEFT JOIN BookingProgramMaster pm ON pm.BookingProgramId = claim.ProgramID
			LEFT JOIN Notifications n on claim.BookingClaimId = n.BookingClaimID

			WHERE ISNULL(b.FirstTechnicianId, 0) =  CASE WHEN @technicianid = 0 THEN ISNULL(b.FirstTechnicianId, 0) ELSE @technicianid END
				AND DATEADD(dd, 0, DATEDIFF(dd, 0, ISNULL(claim.CreatedOn, @StartDate)))
					BETWEEN CASE WHEN @StartDate IS NULL THEN DATEADD(dd, 0, DATEDIFF(dd, 0, (SELECT MIN(CreatedOn) FROM BookingClaims))) ELSE DATEADD(dd, 0, DATEDIFF(dd, 0, @StartDate)) END
						AND CASE WHEN @EndDate IS NULL THEN DATEADD(dd, 0, DATEDIFF(dd, 0, (SELECT MAX(CreatedOn) FROM BookingClaims))) ELSE DATEADD(dd, 0, DATEDIFF(dd, 0, @EndDate)) END
				AND ISNULL(claim.ProgramID, 0) =  CASE WHEN @programid = 0 THEN ISNULL(claim.ProgramID, 0) ELSE @programid END
				--AND (ISNULL(claim.IsValidAccount, 0) <> 0 OR ISNULL(claim.IsEligible, 0) <> 0)

			ORDER BY claim.CreatedOn DESC
		END
			
	ELSE
		BEGIN
			SELECT a.BookingClaimId
				, claim.ConsumerEnergyAccountNumber AS 'GasUtilityAccountNo'
				, CASE WHEN b.Status IS NULL THEN 'Scheduled' WHEN b.Status = 0 THEN 'FT Assigned' ELSE 'Dispatched' END AS [ScheduleType]
				--, CASE WHEN b.Status IS NULL THEN 'Scheduled' WHEN b.Status = 0 THEN 'FT Assigned' ELSE 'Dispatched' END AS [Status]
				, claim.Basement
				, claim.ScheduleDate
				, claim.ScheduleTime
				, ISNULL(u1.FirstName, '') + ' ' + ISNULL(u1.LastName, '') AS [Group]
				, claim.PhoneNo
				, ISNULL(b.[Status], 0) AS [Status]
				, ISNULL(claim.FirstName, '') + ' ' + ISNULL(claim.LastName, '') AS [Client]
				, ISNULL(b.FirstTechnicianId, 0) AS [FirstTechnicianId] --Quick patch to eliminate duplicate booking dispatch
				, b.SecondTechnicianId --Quick patch to eliminate duplicate booking dispatch
				, a.BookingDispatchID --Quick patch to eliminate duplicate booking dispatch
				, b.CreatedOn
				, ISNULL(status.Name, 'Inactive') AS 'DispatchStatusDesc'
				, ISNULL(status.IsPositive, 0) AS 'IsPositive'
				, claim.FTLogFile
				, claim.ProgramID
				, pm.ProgramName

				, claim.BRNumber
				, claim.PremiseID
				, claim.AccountType
				, claim.HEAParticipation
				, claim.IQParticipation
				, claim.EaseOnly
				, claim.PriorParticipationDate
				, n.[Status] AS NotificationStatus
			FROM (
				SELECT MAX(bd.BookingDispatchId) AS [BookingDispatchId]
					, bc.BookingClaimId
				FROM BookingClaims bc
				LEFT JOIN BookingDispatch bd ON bd.BookingClaimId = bc.BookingClaimId
				
				GROUP BY bc.BookingClaimId
				) a
			LEFT JOIN BookingClaims claim ON claim.BookingClaimId = a.BookingClaimId
			LEFT JOIN BookingDispatch b ON b.BookingDispatchId = a.BookingDispatchId
			LEFT JOIN BookingTechnician ft1 ON ft1.TechnicianId = b.FirstTechnicianId
			LEFT JOIN Users u1 ON u1.UserId = ft1.UserId
			LEFT JOIN BookingStatus status ON status.StatusId = b.Status
			LEFT JOIN BookingProgramMaster pm ON pm.BookingProgramId = claim.ProgramID
			LEFT JOIN Notifications n on claim.BookingClaimId = n.BookingClaimID

			WHERE ISNULL(b.FirstTechnicianId, 0) =  CASE WHEN @technicianid = 0 THEN ISNULL(b.FirstTechnicianId, 0) ELSE @technicianid END
				AND DATEADD(dd, 0, DATEDIFF(dd, 0, ISNULL(claim.ScheduleDate, @StartDate)))
					BETWEEN CASE WHEN @StartDate IS NULL THEN DATEADD(dd, 0, DATEDIFF(dd, 0, (SELECT MIN(ScheduleDate) FROM BookingClaims))) ELSE DATEADD(dd, 0, DATEDIFF(dd, 0, @StartDate)) END
						AND CASE WHEN @EndDate IS NULL THEN DATEADD(dd, 0, DATEDIFF(dd, 0, (SELECT MAX(ScheduleDate) FROM BookingClaims))) ELSE DATEADD(dd, 0, DATEDIFF(dd, 0, @EndDate)) END
				AND ISNULL(claim.ProgramID, 0) =  CASE WHEN @programid = 0 THEN ISNULL(claim.ProgramID, 0) ELSE @programid END
				--AND (ISNULL(claim.IsValidAccount, 0) <> 0 OR ISNULL(claim.IsEligible, 0) <> 0)

			ORDER BY claim.ScheduleDate DESC
		END
END



GO


