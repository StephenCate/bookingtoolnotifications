ALTER DATABASE BookingTool SET SINGLE_USER WITH ROLLBACK AFTER 60 SECONDS
 
Alter Database BookingTool SET Enable_Broker; -- this requires killing all existing connections to the DB

CREATE QUEUE NotificationChangeMessages;

CREATE SERVICE NotificationChangeNotifications
ON QUEUE NotificationChangeMessages
 
ALTER DATABASE BookingTool SET MULTI_USER