USE [BookingTool]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGetProgramAbbreviation]    Script Date: 24/01/2016 7:55:29 PM ******/
DROP FUNCTION [dbo].[fnGetProgramAbbreviation]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGetProgramAbbreviation]    Script Date: 24/01/2016 7:55:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Stephen Cate
-- Create date: 01/23/2016
-- Description:	Gets the Abbreviated Program name based on the first letters
-- =============================================
CREATE FUNCTION [dbo].[fnGetProgramAbbreviation] 
(
	-- Add the parameters for the function here
	@ProgramName VARCHAR(500)
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Output VARCHAR(50)

	-- Add the T-SQL statements to compute the return value here
	declare @String varchar(8000)

	set @String = @ProgramName
	
	declare @TextXML xml
	
	select @TextXML = cast('<d>' + replace(@String, ' ', '</d><d>') + '</d>' as xml)
	
	declare @Result varchar(8000)
	
	set @Result = ''
	
	select
	
	@Result = @Result + left(T.split.value('.', 'nvarchar(max)'),1)
	
	from
	
	@TextXML.nodes('/d') T(split)
	
	SET @Output = @Result

	-- Return the result of the function
	RETURN @Output;

END

GO


