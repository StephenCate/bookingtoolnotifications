﻿using BookingNotifications.Core;
using NLog;
using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace BookingNotifications
{
	internal class Program
	{
		private static Logger _logger = LogManager.GetLogger("BookingToolsLogger");

		private static Thread _ConfirmationWorkerThread;

		private static INotificationProcessor _NotificationProcessor;

		private static CancellationTokenSource _Cts = new CancellationTokenSource();

		private static Task _MainTask = null;

		private static void Main(string[] args)
		{
			#region Notificaiton Services

			//Console.WriteLine("Starting the Notifications service...");

			//_logger.Info("Starting the Notifications service...");
			Console.WriteLine("Starting the Notifications service...");

			_MainTask = new Task(StartNotification, _Cts.Token, TaskCreationOptions.LongRunning);

			_MainTask.Start();

			Console.ReadLine();

			#endregion Notificaiton Services
		}

		private static void StartNotification()
		{
			CancellationToken cancellation = _Cts.Token;

			TimeSpan interval = TimeSpan.Zero;

			// 48 Hour Notifications
			var initializer = new NotificationInitializer
			{
				Logger = _logger,
				ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
				PollingInterval = Convert.ToInt32(ConfigurationManager.AppSettings["PollingIntervalInSeconds"].ToString()),
				UserName = ConfigurationManager.AppSettings["EZTextUserName"].ToString(),
				Password = ConfigurationManager.AppSettings["EZTextPassword"].ToString(),
				TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]),
				TestPhoneNumber = ConfigurationManager.AppSettings["TestPhoneNumber"],
				MessageTypeID = ConfigurationManager.AppSettings["EZTextMessageTypeID"].ToString(),
				ApiBaseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"].ToString(),
				ProcessorQuery = string.Format("select NotificationID, Phone, Email, NotificationType, RequestMessage from dbo.Notifications where Status = {0} and LastUpdated IS NULL",
							(int)NotificationStatus.Pending),
				ByPassSmsSendReturnSuccess = Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassSmsSendReturnSuccess"].ToString())
			};

			while (!cancellation.WaitHandle.WaitOne(interval))
			{
				try
				{

					_logger.Info("Begin processing notifications...");

					#region setup the Notification Processor

					_NotificationProcessor = new NotificationsProcessor(initializer);

					_NotificationProcessor.ProcessNotifications();

					#endregion setup the Notification Processor

					// Occasionally check the cancellation state.
					if (cancellation.IsCancellationRequested)
					{
						break;
					}
					interval = new TimeSpan(0, 0, initializer.PollingInterval);

					_logger.Info("Successfully processed batch.");
				}
				catch (Exception ex)
				{
					_logger.Error("There were exceptions caught in processing the last batch.", ex);
					interval = new TimeSpan(0, 0, initializer.PollingInterval);
				}
			}
		}
	}
}